const express = require('express');
const app = express();
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require("../schemas/UserSchema");

// 指定模版引擎
app.set('view engine', 'pug');
app.set('views', 'views');

// localhost:3000/login
router.get('/', (req, res, next) => {
    res.status(200).render('login');
})

/**
 * @route       POST /
 * @description 登录接口
 * @access      public
 */
router.post('/', async(req, res, next) => {
    const payload = req.body;

    // 判断
    if (req.body.logUsername && req.body.logPassword) {
        // 查询数据库
        const user = await User.findOne({
            $or: [
                { username: req.body.logUsername },
                { email: req.body.logUsername },
            ]
        }).catch((err) => {
            payload.errorMessage = "Something went wrong.";
            res.status(400).render("login", payload);
        })

        if(user != null) {
            // 密码匹配
            const result = await bcrypt.compare(req.body.logPassword, user.password);

            if (result === true) {
                req.session.user = user;
                return res.redirect("/");
            }
        }

        payload.errorMessage = "username or email or password is incorrect";
        return res.status(400).render('login', payload);
    }
    console.log(payload);
    res.status(200).render('login');
})

module.exports = router;
