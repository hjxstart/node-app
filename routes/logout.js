const express = require('express');
const router = express.Router();

/**
 * @router      GET /
 * @description 退出登录接口
 * @access      public
 */
router.get('/', (req, res, next) => {
    if (req.session) {
        req.session.destroy(() => {
            res.redirect('/login');
        })
    }
})

module.exports = router;
