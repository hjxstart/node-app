const express = require('express');
const router = express.Router();
const Post = require('../../schemas/PostSchema');
const User = require('../../schemas/UserSchema');

/**
 * @router      GET /
 * @description 获取信息接口
 * @access      public
 */
router.get('/', ((req, res, next) => {
    Post.find().then(results => {
        res.status(200).send(results);
    }).catch(err => {
        res.sendStatus(400).json(err);
    })
}))

/**
 * @router      POST /
 * @description POSTS接口
 * @access      public
 */
router.post('/', (req, res, next) => {
    // 判断前端是否传递参数
    if (!req.body.content) {
        return res.status(400).json({error: "params is valid"});
    }

    // 参数正确 准备插入数据
    var postData = {
        content: req.body.content,
        postedBy: req.session.user
    }

    // 插入数据到数据库
    Post.create(postData).then(async (newPost) => {
        // 加入user
        newPost = await User.populate(newPost, {path: "postedBy"})
        res.status(201).send(newPost);
    }).catch(err => {
        res.status(400).json({error: "insert the port is failed" + err})
    })

})

module.exports = router;
