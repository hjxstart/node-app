const mongoose = require('mongoose');

class Database {

    constructor() {
        this.connect();
    }
    connect() {
        mongoose.connect('mongodb://172.16.3.9:27017/blog').then(() => {
            console.log('database connection successful');
        }).catch((err) => {
            console.log('database connection error' + err);
        })
    }
}

module.exports = new Database();
