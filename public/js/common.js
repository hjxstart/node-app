$('#postTextarea').keyup(event => {
    let textbox = $(event.target);
    let value = textbox.val().trim();

    var submitButton = $('#submitPostButton');

    if (value == '') {
        submitButton.prop('disabled', true);
        return;
    }

    submitButton.prop('disabled', false);
})

$('#submitPostButton').click(() => {
    var button = $(event.target);
    var textbox = $('#postTextarea');
    var data = {
        content: textbox.val()
    }

    // 发送请求
    // /api/posts == http://localhost:3000/api/posts
    $.post('/api/posts', data, (postData, status, xhr) => {
        var html = createPostHtml(postData);
        $(".postContainer").prepend(html);
        textbox.val('');
        button.prop('disabled', true);
    })
})

function createPostHtml(postData) {

    var postedBy = postData.postedBy;

    return `<div class="post">
        <div class="mainContentContainer">
            <!-- 左边 -->
            <div class="userImageContainer">
                <img src="${postedBy.profilePic}">
            </div>
            <!-- 右边 -->
            <div class="postContentContainer">
                <!-- 头部 -->
                <div class="header">
                    <a href="/profile/${postedBy.username}" class="displayName">${postedBy.name}</a>
                    <span class="username">@${postedBy.username}</span>
                    <span class="date">${postedBy.createdAt}</span>
                </div>
                <!-- 主体 -->
                <div class="postBody">
                    <span>${postData.content}</span>
                </div>
                <!-- footer -->
                <div class="postFooter">
                    <div class="postButtonContainer">
                        <button>
                            <i class="fa fa-comment"></i>
                        </button>
                    </div>
                    <div class="postButtonContainer">
                        <button>
                            <i class="fa fa-retweet"></i>
                        </button>
                    </div>
                    <div class="postButtonContainer">
                        <button>
                            <i class="fa fa-heart"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>`;
}
