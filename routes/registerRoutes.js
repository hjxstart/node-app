const express = require('express');
const app = express();
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../schemas/UserSchema');

// 指定模版引擎
app.set('view engine', 'pug');
app.set('views', 'views');

// localhost:3000/register
/**
 * @route       GET /
 * @description 渲染注册页面
 * @access      public
 */
router.get('/', (req, res, next) => {
    res.status(200).render('register');
})
/**
 * @route       POST /
 * @description 注册接口
 * @access      public
 */
router.post('/', async (req, res, next) => {
    const name = req.body.name.trim();
    const username = req.body.username.trim();
    const email = req.body.email.trim();
    const password = req.body.password;

    let payload = req.body;

    if(name && username && email && password) {
        // 存储

        // 查询 数据库是否存储username 或者 email
        const user = await User.findOne({
            $or: [
                { username },
                { email },
            ]
        }).catch(err => {
                payload.errorMessage = "Something went wrong."
            })

        // 判断
        if (user == null) {
            // 没有查到 正常存

            // 密码加密
            const data = req.body;
            data.password = await bcrypt.hash(password, 10);

            User.create(data).then(user => {
                // 配置session
                req.session.user = user;
                return res.redirect('/');
            }).catch(err => {
                console.log(err);
            })

        } else {
            // 查到 跟数据库有冲突
            if (email == user.email) {
                payload.errorMessage = "Email already in use";
            } else {
                payload.errorMessage = "Username already in use";
            }

            res.status(400).render('register', payload);
        }

    }else {
        payload.errorMessage = 'Make sure each filed has a valid value.'
        res.status(200).render('register', payload);
    }
})

module.exports = router;
