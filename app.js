const express = require('express');
const app = express();
const port = 3000;
const path = require('path');
const session = require('express-session');
const middleware = require('./middleware');
const database = require('./databases');

const server = app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
})

// 配置post接收的数据类型
app.use(express.json()); // 客户端发来的是json数据，那么就正常接收
app.use(express.urlencoded({extended: false})); // x-www-form-urlencoded

// 使用session
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: false
}))

// 设置模版引擎
app.set('view engine', 'pug');
// 设置views的文件夹名称
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));

// localhost:3000/
app.get('/', middleware.requireLogin, (req, res, next) => {

    const payload = {
        pageTitle: '标题',
        userLoggedIn: req.session.user
    }

    res.status(200).render('home', payload);
})

// 关联路由
const loginRoutes = require('./routes/loginRoutes');
const registerRoutes = require('./routes/registerRoutes');
const logoutRoutes = require('./routes/logout');

// Api routes
const postsApiRoutes = require('./routes/api/posts');

// localhost:3000/login
app.use('/login', loginRoutes);
app.use('/register', registerRoutes);
app.use('/logout', logoutRoutes);

// Api use
app.use('/api/posts', postsApiRoutes);


